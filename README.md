# CI/CD Testing
[![Build Status](https://semaphoreci.com/api/v1/rsfxiii/cicd/branches/master/badge.svg)](https://semaphoreci.com/rsfxiii/cicd)
[![Docker Automated build](https://img.shields.io/docker/automated/jrottenberg/ffmpeg.svg)](https://hub.docker.com/r/raijinn/cicd)
![Python](https://img.shields.io/badge/python-3.5.2-brightgreen.svg)

### Dev & Deploy Flow ###
- Developer: `git push [fork] [feature_branch]`
- Semaphore: Receives build request from GitHub webhook
  - Slack: Receives notification from Semaphore webhook
  - Tests:
    - Types: Syntax checker, unit tests, integration tests
    - _On Success_:
      - Send success notification to Slack channel
        - _On Deploy_:
          - ???
          - Profit
    - _On Failure_:
      - Slack: Receives notification of animu monkey vomiting and nothing else happens.
        - Should include a link to the expanded traceback in Semaphore.


#### De-scoped (Not Doing) ####
- Notification includes `Veto` button with 10s timer available before
  merge, build, & deployment
- Slack Webhook:
  - _On Veto_:
    - Delay auto-merge for X minutes; requires text entry from voto'er.
