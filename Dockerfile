FROM oldhammade/alpine-pyenv-py3.5.2
ENV AUTHOR=rsfxiii

WORKDIR /CICD
COPY . /CICD
RUN pwd
RUN pyenv install -s 3.5.2
ENTRYPOINT . script/test
